Обновление CentOS

```yum -y update``` 

Пара полезных программ, nano если vi не устраивает, htop лучше просто top

```yum install -y nano htop```  

Сразу подключаем EPEL

```yum install -y epel-release```

Обновление с новым репозиторием

```yum -y update```

Установка x2go

```yum install -y x2goserver x2goserver-xsession x2goserver-fmbindings cups-x2go x2goserver-printing```

Установка Xfce

```yum groupinstall -y "Xfce"```

```yum groupinstall -y "X Window system"```

Перезагрузка

```reboot```

В ISP-панели через ssh выполняем

Маскируем NM, иначе он сломает сеть после перезагрузки

```systemctl disable NetworkManager.service```

```systemctl mask NetworkManager.service```

```systemctl disable NetworkManager-wait-online.service```

```systemctl mask NetworkManager-wait-online.service```

Перезагрузка

```reboot```

Пробуем подключиться x2go клиентом к серверу.